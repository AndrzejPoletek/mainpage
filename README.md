# Welcome to my portfolio https://gitlab.poletek.net

## About me
Hello! My name is Andrzej Połetek.

I'm a programmer with a passion for full-stack development and DevOps methodology.
I'm enthusiastic about technology and initially took my first steps as a backend developer.
I wrote my first lines of code when I was 13 years old, and this journey continues to this day.

## My technology stack:
Below, I have listed my technology stack sorted by categories of tools.

- **Backend:** Python, Django, Django Rest Framework, Django Graphene, Celery
- **Frontend:** React JS, TypeScript, AntDesign, jQuery
- **Databases:** Redis, Rethink, Postgres, MySQL
- **DevOps:** Ubuntu Server, Libvirt, Ansible, Gitlab CI/CD, Nginx, OpenVPN, Docker, Docker Compose
- **Other:** RabbitMQ, SonarQube, NextCloud, Redmine

# Projects

## CyborgSolutions
### Description:
CyborgSolutions is a virtual company created exclusively for training purposes in IT solutions that are essential in a real-world company setting.   
It serves as an invaluable context for enhancing practical skills within a homelab environment.  
CyborgSolutions functions as a unique entity catering to robots, where individuals provide various services for them.

Also its a small training tech lead skills and combine all my tech skills.  
Im active looking for developers to contributing together projects which we do in this virtual organization.  
More about projects made by this virtual organization is in description from below link.

**Link to description:** https://gitlab.com/portfolio133/cyborgsolutions/cyborgsolutions_main  

### Projects
#### CyborgMessenger
The "CyborgMessenger" project is an advanced internet messenger designed using various technologies to provide a comprehensive and intuitive platform for user communication.   
**Docs:** [Link](https://gitlab.com/portfolio133/cyborgsolutions/cyborgmessenger/cyborgmessenger_docs)  
**Backend:** [Link](https://gitlab.com/portfolio133/cyborgsolutions/cyborgmessenger/cyborgmessenger_backend)  
**Frontend:** [Link](https://gitlab.com/portfolio133/cyborgsolutions/cyborgmessenger/cyborgmessenger_frontend)  
### Contributors:
1. [Andrzej Połetek](https://gitlab.poletek.net) - As a CEO, techlead, and programmer
2. [Ewa Kucała](https://github.com/Ewa-Anna) - Backend Developer



## Driving Robot Arduino
### Description:
One time I tried to play with Arduino and made a robot, changing an old PC pad to a steering pilot. In the repository, you can find code for the robot, steering pad, and a video with the finished product that worked.  
**Tech Stack:** Arduino  
**Link:** https://gitlab.com/portfolio133/drivingrobot

## Weather station Arduino
### Description
Another project based on Arduino, just a simple weather station.  
**Tech Stack:** Arduino  
**Link:** https://gitlab.com/portfolio133/weatherstation

## Resetter Orange Pi
### Description
One day my boss came to me and said, "Hey Andrew, I have a problem with my router; sometimes it freezes, and I lose connection to the internet." So I said, "Okay, I'll find some solution for this."
And I made a Resetter; it can steer a 230AC device by pinging 10 biggest websites on the internet. If any of them do not respond, the system will restart the device by cutting off the power and restoring power.
Inside, you can find the schema and code.
Finally, it wasn't deployed in production, but I used this device to control an oil furnace in my home to reduce oil usage when warm water is not needed.  
**Tech Stack:** Python, OrangePi  
**Link:** https://gitlab.com/portfolio133/reseter_orangepi